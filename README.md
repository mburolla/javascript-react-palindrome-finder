# JavaScript React: Palindrome Finder Using Recoil

![](./docs/palindrome-finder.png)

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Start the app: `npm start`

# Architecture
![](./docs/tree.png)

![](./docs/overview.png)


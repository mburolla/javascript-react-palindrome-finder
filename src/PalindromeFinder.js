import React, { useRef, useState } from 'react'
import { useRecoilState } from 'recoil'
import { palindromeCount, sentenceCount, sentences } from './atoms'
import { findPalindromes } from './Palindrome'

import './PalindromeFinder.scss'

export const PalindromeFinder = () => {
    let inputRef = useRef(null)
    const [thePalindromeCount, setThePalindromeCount] = useRecoilState(palindromeCount)
    let [theSentenceCount, setSentenceCount] = useRecoilState(sentenceCount)
    let [theSentences, setSentences] = useRecoilState(sentences)
    let [palidromes, setPalindromes] = useState("")
    let [isFindDisabled, setIsFindDisabled] = useState(true)

    const onFindButtonClick = () => {
        // Find palindromes
        const inputSentence = inputRef.current.value
        const palidromes = findPalindromes(inputSentence)
        setThePalindromeCount(palidromes.length + thePalindromeCount)
        setSentenceCount(++theSentenceCount)
        // Handle sentence history
        let newSentenceArray = [...theSentences]
        newSentenceArray.push({ id: theSentences.length + 1, text: inputSentence })
        setSentences(newSentenceArray)
        // UI
        inputRef.current.value = ""
        if (palidromes.length > 0 ) {
            setPalindromes(palidromes)
        } else {
            setPalindromes(null)
        }
        setIsFindDisabled(true)
    }

    const onInputChanged = () => {
        setIsFindDisabled(false)
    }

    return (
        <div className='PalindromeFinder'>
            <input onChange={() => onInputChanged() } ref={inputRef}></input>
            <button onClick={() => onFindButtonClick()} disabled={isFindDisabled}>Find</button>
            {
                palidromes && <span className='palindromeList'>{ palidromes.join(', ') }</span>
            }
        </div>
    )
}

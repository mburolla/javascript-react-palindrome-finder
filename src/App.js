import './App.scss'
import { Header } from './Header'
import { PalindromeFinder } from './PalindromeFinder'
import { PalindromeHistory } from './PalindromeHistory'

function App() {
  return (
    <div className="App">
      <Header />
      <PalindromeFinder />
      <PalindromeHistory />
    </div>
  );
}

export default App;

import React from 'react'
import { sentences } from './atoms'
import { useRecoilState } from 'recoil'

import './PalindromeHistory.scss'

export const PalindromeHistory = () => {
  let [theSentences] = useRecoilState(sentences)

  return (
    <div className='PalindromeHistory'>
        <h2>Sentence History</h2>  
        { 
          theSentences.map(i => (
            <div key={i.id}>{i.text}</div>
          )) 
        }
    </div>
  )
}

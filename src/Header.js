import React from 'react'

import './Header.scss'
import { Summary } from './Summary'

export const Header = () => {
  return (
    <div className='Header'>
        <div>
            <h2>Palindrome Finder</h2>
        </div>
        <Summary/>
    </div>
  )
}

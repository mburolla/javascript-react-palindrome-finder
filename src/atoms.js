import { atom } from 'recoil'

export const palindromeCount = atom({
    key: 'palindromeCount',
    default: 0
})

export const sentenceCount = atom({
    key: 'sentenceCount',
    default: 0
})

export const sentences = atom({
    key: 'sentences',
    default: []
})

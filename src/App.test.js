const { findPalindromes } = require('./Palindrome')

test('Find radar', () => {
  let results = findPalindromes('radar')
  expect(results.length).toBe(1);
});

test('Find RadaR', () => {
  let results = findPalindromes('RadaR')
  expect(results.length).toBe(1);
});

test('Find two', () => {
  let results = findPalindromes('Radar mom this is a test')
  expect(results.length).toBe(2);
});

test('Find none', () => {
  let results = findPalindromes('There is nothing to see here.')
  expect(results.length).toBe(0);
});

import React from 'react'
import { useRecoilState } from 'recoil'
import { palindromeCount, sentenceCount } from './atoms'

import './Summary.scss'

export const Summary = () => {
    let [thePalindromeCount] = useRecoilState(palindromeCount)
    let [theSentenceCount] = useRecoilState(sentenceCount)
    
    return (
        <div className='Summary'>
            <span>Sentences: { theSentenceCount }</span>&nbsp;
            <span>Palindromes: { thePalindromeCount }</span>
        </div>
    )
}

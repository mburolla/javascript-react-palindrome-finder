//
// File: Palindrome.js
// Auth: Martin Burolla
// Date: 10/7/2022
// Desc: CommonJS module that finds all palindromes in a sentence.
//

//
// Public
//

exports.findPalindromes = (sentence) => {
    const retval = []
    const array = sentence.split(" ");
    array.forEach(word => {
        if (isPalindrome(word)) {
            retval.push(word)
        }
    });
    return retval;
}

//
// Private
//

const isPalindrome = (word) => {
    let retval = false
    if (word.length <= 2) {
        return retval
    }
    word = word.toLowerCase()
    let firstHalf = ""
    let secondHalf = ""
    let midIndex = Math.round((word.length) / 2)
    
    let index = 0;
    while (index < midIndex) {
        firstHalf += word.charAt(index)
        index++;
    }

    index = word.length
    let offset = 0
    if (word.length % 2 === 0) { // Handle even number of characters.
        offset = 1
    }
    while (midIndex <= index - offset) {
        secondHalf += word.charAt(index-1)
        index--;
    }

    if (firstHalf === secondHalf) {
       retval = true
    } 
    return retval
}
